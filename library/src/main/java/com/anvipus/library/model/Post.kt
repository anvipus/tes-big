package com.anvipus.library.model

import android.os.Parcel
import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@JsonClass(generateAdapter = true)
@Parcelize
data class Post (
    @Json(name = "userId")
    val userId: Int?,
    @Json(name = "id")
    val id: Int?,
    @Json(name = "title")
    val title: String?,
    @Json(name = "body")
    val body: String?,
) : Parcelable {
    val viewId get() = "ID : "+id
    val viewTitle get() = "Title : "+title

    val viewBody get() = "Body : "+body
}