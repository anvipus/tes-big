package com.anvipus.explore.repo

import androidx.lifecycle.LiveData
import com.anvipus.explore.api.AuthApi
import com.anvipus.library.api.ApiCall
import com.anvipus.library.model.Post
import retrofit2.Call
import com.anvipus.library.model.Resource
import javax.inject.Inject

class MainRepo @Inject constructor(
    private val api: AuthApi
){
    fun getListPost(): LiveData<Resource<List<Post>>> = object : ApiCall<List<Post>, List<Post>>(){
        override fun createCall(): Call<List<Post>> {
            return api.getListPost()
        }
    }.asLiveData()
}