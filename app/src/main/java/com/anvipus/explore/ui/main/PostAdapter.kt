package com.anvipus.explore.ui.main

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.anvipus.explore.R
import com.anvipus.explore.databinding.ItemPostBinding
import com.anvipus.library.model.Post

class PostAdapter(
    private val itemClickCallback: (Post) -> Unit
): ListAdapter<Post, RecyclerView.ViewHolder>(COMPARATOR){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_post, parent, false)
        return ViewHolder(ItemPostBinding.bind(view))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val data = getItem(position)
        (holder as ViewHolder).run {
            bind(data)
            itemView.setOnClickListener {
                itemClickCallback.invoke(data)
            }
        }
    }
    class ViewHolder(private val binding: ItemPostBinding): RecyclerView.ViewHolder(binding.root){
        fun bind(data: Post?){
            binding.data = data
        }
    }

    companion object{
        const val TAG:String = "PostAdapter"
        val COMPARATOR = object : DiffUtil.ItemCallback<Post>(){
            override fun areItemsTheSame(oldItem: Post, newItem: Post): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Post, newItem: Post): Boolean {
                return oldItem == newItem
            }

        }
    }
}