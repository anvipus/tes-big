package com.anvipus.explore.api

import com.anvipus.library.model.Post
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.QueryMap

interface AuthApi {
    @GET("posts")
    fun getListPost(): Call<List<Post>>
}